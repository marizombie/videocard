﻿using System.Windows;

namespace VideoCard
{
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        private void Name_GotFocus(object sender, RoutedEventArgs e)
        {
            ((MainWindowViewModel)DataContext).ShowHint = false;
        }
    }
}
