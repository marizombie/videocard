﻿using System;
using System.Windows.Input;
using Caliburn.Micro;
using VideoCard.ViewModels;

namespace VideoCard
{
    public class MainWindowViewModel : PropertyChangedBase
    {
        private bool _showHint;
        public bool ShowHint
        {
            get => _showHint;
            set
            {
                _showHint = value;
                NotifyOfPropertyChange(() => ShowHint);
            }
        }

        private string _language;
        public string Language
        {
            get => _language;
            set
            {
                _language = value;
                NotifyOfPropertyChange(() => Language);
            }
        }

        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                if (!string.IsNullOrEmpty(_name))
                    ShowHint = false;
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => ToContinue);
            }
        }

        private string _phone;
        public string Phone
        {
            get => _phone;
            set
            {
                _phone = value;
                if (!string.IsNullOrEmpty(_phone))
                    ShowHint = false;
                NotifyOfPropertyChange(() => Phone);
                NotifyOfPropertyChange(() => ToContinue);
            }
        }

        public bool ToContinue => !string.IsNullOrEmpty(Name) &&
                                  !string.IsNullOrEmpty(Phone) &&
                                  !Phone.Contains("_");
        
        public MainWindowViewModel()
        {
            ShowHintCommand = new RelayCommand(ShowHintExecute, CanShowHint);
            ContinueCommand = new RelayCommand(ContinueExecute, CanContinue);
        }

        public ICommand ShowHintCommand { get; set; }
        public ICommand ContinueCommand { get; set; }

        public bool CanContinue() => ToContinue;

        private void ContinueExecute()
        {
            
        }

        private bool CanShowHint() => string.IsNullOrEmpty(Name) ||
                                   string.IsNullOrEmpty(Phone) ||
                                   Phone.Contains("_");

        private void ShowHintExecute()
        {
            ShowHint = true;
        }
    }
}
